Useful links: \
https://exploreflask.com/en/latest/organizing.html \
https://stackoverflow.com/questions/14415500/common-folder-file-structure-in-flask-app \
https://blog.miguelgrinberg.com/post/the-flask-mega-tutorial-part-xv-a-better-application-structure \
https://www.patricksoftwareblog.com/structuring-a-flask-project/ \
https://www.patricksoftwareblog.com/structuring-a-flask-project/

Quick start:

pip install -r requirements.txt

create DB:

python \
from project import db \
db.create_all() \
exit()

set flask variables

set FLASK_APP=project \
set FLASK_ENV=development

flask run