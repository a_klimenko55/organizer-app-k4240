from sqlalchemy.orm import backref

from project import db
from passlib.hash import pbkdf2_sha256


class DBMixin:
    def save_to_db(self):
        db.session.add(self)
        db.session.commit()

    def delete(self):
        db.session.delete(self)
        db.session.commit()


class User(db.Model, DBMixin):
    __tablename__ = 'users'

    id = db.Column(db.Integer, primary_key=True)
    email = db.Column(db.String(120), unique=True, nullable=False)
    password = db.Column(db.String(200), nullable=False)
    folders = db.relationship('Folder', backref='user', lazy='dynamic')
    actions = db.relationship('UserActionLog', backref='user')

    def __init__(self, email, password):
        self.email = email
        self.password = pbkdf2_sha256.hash(password)

    def check_password(self, password):
        return pbkdf2_sha256.verify(password, self.password)

    def __repr__(self):
        return f'<User {{ email: {self.email}, \
    password: {self.password} }}'


class Folder(db.Model, DBMixin):
    __tablename__ = 'folders'

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(255), nullable=False)
    parent_id = db.Column(db.Integer, db.ForeignKey('folders.id'), index=True, nullable=True)
    parent = db.relationship('Folder', remote_side=id, backref=backref('sub_folders', cascade="all,delete"))
    # sub_folders = db.relationship('Folder', back_populates='parent')
    # parent = db.relationship('Folder', remote_side=id, back_populates='sub_folders', single_parent=True, cascade='all, delete-orphan')
    user_id = db.Column(db.Integer, db.ForeignKey('users.id'), nullable=False)
    tasks = db.relationship('Task', backref='folder', cascade="all,delete", lazy=True)

    def __init__(self, name, parent_id, user_id):
        self.name = name
        self.parent_id = parent_id
        self.user_id = user_id


class Task(db.Model, DBMixin):
    __tablename__ = 'tasks'

    id = db.Column(db.Integer, primary_key=True)
    date = db.Column(db.DateTime, nullable=True)
    name = db.Column(db.String(255), nullable=False)
    description = db.Column(db.Text(), nullable=True)
    folder_id = db.Column(db.Integer, db.ForeignKey('folders.id'), nullable=False)
    files = db.relationship('File', backref='task', cascade="all,delete", lazy=True)
    status = db.Column(db.Boolean, default=False, nullable=False)

    def __init__(self, date,  name, description, folder_id):
        self.date = date
        self.name = name
        self.description = description
        self.folder_id = folder_id


class File(db.Model, DBMixin):
    __tablename__ = 'files'

    id = db.Column(db.Integer, primary_key=True)
    filename = db.Column(db.String(255), nullable=False)
    task_id = db.Column(db.Integer, db.ForeignKey('tasks.id'), nullable=False)

    def __init__(self, filename,  task_id):
        self.filename = filename
        self.task_id = task_id


class TokenBlacklist(db.Model, DBMixin):
    id = db.Column(db.Integer, primary_key=True)
    jti = db.Column(db.String(36), nullable=False, unique=True)
    token_type = db.Column(db.String(10), nullable=False)
    user_identity = db.Column(db.String(50), nullable=False)
    revoked = db.Column(db.Boolean, nullable=False)
    expires = db.Column(db.DateTime, nullable=False)

    def to_dict(self):
        return {
            'token_id': self.id,
            'jti': self.jti,
            'token_type': self.token_type,
            'user_identity': self.user_identity,
            'revoked': self.revoked,
            'expires': self.expires
        }


class UserActionLog(db.Model):
    __tablename__ = 'user_action_logs'

    id = db.Column(db.Integer, primary_key=True)
    user_id = db.Column(db.Integer, db.ForeignKey('users.id'), nullable=False)
    action = db.Column(db.Text, nullable=False)
