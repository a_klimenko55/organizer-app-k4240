from project import ma
from project.models import Task, Folder


class TaskSchema(ma.SQLAlchemyAutoSchema):
    class Meta:
        model = Task
        include_fk = True
        datetimeformat = '%d-%m-%Y %H:%M:%S'


class FolderSchema(ma.SQLAlchemyAutoSchema):
    # A list of author objects
    tasks = ma.Nested(TaskSchema, many=True)
    parent = ma.auto_field()
    sub_folders = ma.auto_field()
    class Meta:
        model = Folder
