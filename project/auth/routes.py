from functools import wraps

from flasgger import swag_from
from flask import request, jsonify, render_template, redirect, url_for
from sqlalchemy.orm.exc import NoResultFound
from project import jwt
from project.auth import bp
from project.blacklist_helpers import add_token_to_database
from project.models import User, Folder, TokenBlacklist
from flask_jwt_extended import (create_access_token, create_refresh_token, jwt_refresh_token_required,
                                get_jwt_identity, get_raw_jwt, jwt_required)


def login_validation(func):
    @wraps(func)
    def wrapper(*args, **kwargs):
        if not request.is_json:
            return jsonify({"msg": "Missing JSON in request"}), 400

        email = request.json.get('email', None)
        password = request.json.get('password', None)

        if not email:
            return jsonify({"msg": "Missing email parameter"}), 400
        if not password:
            return jsonify({"msg": "Missing password parameter"}), 400
        kwargs['email'] = email
        kwargs['password'] = password
        return func(*args, **kwargs)
    return wrapper


@bp.route('/login', methods=['POST'])
@swag_from('apidocs/register_and_login.yml')
@login_validation
def login(email, password):
    user = User.query.filter_by(email=email).first()

    if not user or not user.check_password(password=password):
        return jsonify({"msg": "Incorrect email or password"}), 401

    # Use create_access_token() and create_refresh_token() to create our
    # access and refresh tokens
    ret = {
        'access_token': create_access_token(identity=user.email),
        'refresh_token': create_refresh_token(identity=user.email)
    }

    # Store the tokens in our store with a status of not currently revoked.
    add_token_to_database(ret['access_token'], 'identity')
    add_token_to_database(ret['refresh_token'], 'identity')

    return jsonify(ret), 200


@bp.route('/register', methods=['POST'])
@swag_from('apidocs/register_and_login.yml')
@login_validation
def register(email, password):
    # create new user
    new_user = User(email, password)
    new_user.save_to_db()

    root_folder = Folder(name='root', parent_id=None, user_id=new_user.id)
    root_folder.save_to_db()

    ret = {
        'access_token': create_access_token(identity=new_user.email),
        'refresh_token': create_refresh_token(identity=new_user.email)
    }

    # Store the tokens in our store with a status of not currently revoked.
    add_token_to_database(ret['access_token'], 'identity')
    add_token_to_database(ret['refresh_token'], 'identity')

    return jsonify(ret), 200


# Refresh token endpoint. This will generate a new access token from
# the refresh token, but will mark that access token as non-fresh,
# as we do not actually verify a password in this endpoint.
@bp.route('/refresh', methods=['POST'])
@jwt_refresh_token_required
@swag_from('apidocs/refresh.yml')
def refresh():
    current_user = get_jwt_identity()
    new_token = create_access_token(identity=current_user, fresh=False)
    ret = {'access_token': new_token}
    add_token_to_database(ret['access_token'], 'identity')
    return jsonify(ret), 200


# Fresh login endpoint. This is designed to be used if we need to
# make a fresh token for a user (by verifying they have the
# correct email and password). Unlike the standard login endpoint,
# this will only return a new access token, so that we don't keep
# generating new refresh tokens, which entirely defeats their point.
@bp.route('/fresh-login', methods=['POST'])
@swag_from('apidocs/fresh_login.yml')
@login_validation
def fresh_login(email, password):
    user = User.query.filter_by(email=email).first()
    if not user or not user.check_password(password=password):
        return jsonify({"msg": "Incorrect email or password"}), 401

    new_token = create_access_token(identity=email, fresh=True)
    ret = {'access_token': new_token}

    add_token_to_database(ret['access_token'], 'identity')

    return jsonify(ret), 200


# Endpoint for revoking the current users access token
@bp.route('/logout', methods=['DELETE'])
@jwt_required
@swag_from('apidocs/logout.yml')
def logout():
    jti = get_raw_jwt()['jti']
    identity = get_jwt_identity()
    token = TokenBlacklist.query.filter_by(jti=jti, user_identity=identity).one()
    token.revoked = True
    token.save_to_db()

    return jsonify({"msg": "Successfully logged out"}), 200


# Endpoint for revoking the current users refresh token
@bp.route('/logout2', methods=['DELETE'])
@jwt_refresh_token_required
@swag_from('apidocs/logout2.yml')
def logout2():
    jti = get_raw_jwt()['jti']
    identity = get_jwt_identity()
    token = TokenBlacklist.query.filter_by(jti=jti, user_identity=identity).one()
    token.revoked = True
    token.save_to_db()

    return jsonify({"msg": "Successfully logged out"}), 200


@bp.route('/apidocs/login', methods=['GET', 'POST'])
def apidocs_login():
    if request.method == 'POST':

        email = request.form.get('email')
        password = request.form.get('password')

        user = User.query.filter_by(email=email).first()

        if not user or not user.check_password(password=password):
            return jsonify({"msg": "Incorrect email or password"}), 401

        access_token = create_access_token(identity=user.email)

        response = redirect(url_for('flasgger.apidocs'))
        response.set_cookie('access_token_cookie', access_token)
        add_token_to_database(access_token, 'identity')

        return response

    return render_template('auth/login.html')


@jwt.token_in_blacklist_loader
def check_if_token_in_blacklist(decrypted_token):
    jti = decrypted_token['jti']
    try:
        token = TokenBlacklist.query.filter_by(jti=jti).one()
    except NoResultFound:
        return True
    return token.revoked

