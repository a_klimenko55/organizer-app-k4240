from functools import wraps

from flasgger import swag_from
from flask_jwt_extended import jwt_required, get_jwt_identity
from sqlalchemy.exc import DataError
from sqlalchemy.orm.exc import NoResultFound
from project.api import bp
from project.models import Task, User, Folder
from project.schemas import TaskSchema, FolderSchema
from flask import jsonify, request
from datetime import datetime


def task_validation(func):
    @wraps(func)
    def wrapper(*args, **kwargs):
        date = request.json.get('date', None)
        if date:
            try:
                date = datetime.strptime(date, '%d-%m-%Y %H:%M:%S')
            except ValueError:
                return jsonify(msg='Unacceptable datetime format'), 422
        name = request.json.get('name', None)
        if not isinstance(name, str):
            return jsonify(msg='Name is required and must be a string'), 422

        description = request.json.get('description', None)

        folder_id = request.json.get('folder_id', None)

        user = get_current_user()
        try:
            folder = user.folders.filter(Folder.id == folder_id).one()
        except NoResultFound:
            return jsonify(msg='Folder not found for this user'), 404
        kwargs['date'] = date
        kwargs['name'] = name
        kwargs['description'] = description
        kwargs['folder_id'] = folder.id
        return func(*args, **kwargs)

    return wrapper


def folder_validation(func):
    @wraps(func)
    def wrapper(*args, **kwargs):
        user = get_current_user()
        name = request.json.get('name', None)
        if not isinstance(name, str):
            return jsonify(msg='Name is required and must be a string'), 422
        parent_id = request.json.get('parent_id', None)
        try:
            parent = user.folders.filter(Folder.id == parent_id).one()
        except NoResultFound:
            return jsonify(msg='Parent folder not found for this user'), 404
        kwargs['name'] = name
        kwargs['parent_id'] = parent.id
        kwargs['user'] = user
        return func(*args, **kwargs)

    return wrapper


def get_current_user():
    # Access the identity of the current user with get_jwt_identity
    current_user_email = get_jwt_identity()
    return User.query.filter_by(email=current_user_email).first()


# index
@bp.route('/', methods=['GET'])
@jwt_required
def index():
    return 'There is api', 200


# get tasks list
@bp.route('/tasks', methods=['GET'])
@jwt_required
@swag_from('apidocs/get_tasks.yml')
def get_tasks():
    if not request.is_json:
        return jsonify({"msg": "Missing JSON in request"}), 400
    folder_id = request.json.get('folder_id', None)
    user = get_current_user()
    if not folder_id:
        # get root folder for current user
        root_folder = Folder.query.filter_by(parent_id=None, user_id=user.id).first()
        tasks = Task.query.filter_by(folder_id=root_folder.id).all()
    else:
        # check that this folder belongs to current user
        try:
            folder = user.folders.filter(Folder.id == folder_id).one()
        except NoResultFound:
            return jsonify(msg='Folder not found for this user'), 404
        tasks = Task.query.filter_by(folder_id=folder.id).all()

    tasks_schema = TaskSchema(many=True)
    return jsonify(tasks_schema.dump(tasks)), 200


# get task
@bp.route('/tasks/<int:task_id>', methods=['GET'])
@jwt_required
@swag_from('apidocs/get_task.yml')
def get_task(task_id):
    user = get_current_user()
    try:
        task = Task.query.join(Folder, Task.folder_id == Folder.id)\
            .join(User, Folder.user_id == user.id)\
            .filter(Task.id == task_id).one()
    except NoResultFound:
        return jsonify(msg='Task not found for this user'), 404

    task_schema = TaskSchema()
    return jsonify(task_schema.dump(task)), 200


# create task
@bp.route('/tasks', methods=['POST'])
@jwt_required
@swag_from('apidocs/create_task.yml')
@task_validation
def create_task(date, name, description, folder_id):
    new_task = Task(date=date, name=name, description=description, folder_id=folder_id)
    try:
        new_task.save_to_db()
    except DataError:
        return jsonify(msg='Data is incorrect'), 422

    # return jsonify(msg='Success'), 200
    task_schema = TaskSchema()
    return jsonify(task_schema.dump(new_task)), 200


# update task
@bp.route('/tasks/<int:task_id>', methods=['PUT'])
@jwt_required
@swag_from('apidocs/update_task.yml')
@task_validation
def update_task(task_id, date, name, description, folder_id):
    task = Task.query.filter_by(id=task_id).first()
    task.date = date
    task.name = name
    task.description = description
    task.folder_id = folder_id
    task.save_to_db()

    task_schema = TaskSchema()
    return jsonify(task_schema.dump(task)), 200


# delete task
@bp.route('/tasks/<int:task_id>', methods=['DELETE'])
@jwt_required
@swag_from('apidocs/delete_task.yml')
def delete_task(task_id):
    user = get_current_user()
    try:
        task = Task.query.join(Folder, Task.folder_id == Folder.id) \
            .join(User, Folder.user_id == user.id) \
            .filter(Task.id == task_id).one()
    except NoResultFound:
        return jsonify(msg='Task not found for this user'), 404
    task.delete()

    task_schema = TaskSchema()
    return jsonify(task_schema.dump(task)), 200


# get folders list
@bp.route('/folders', methods=['GET'])
@jwt_required
@swag_from('apidocs/get_folders.yml')
def get_folders():
    user = get_current_user()
    folders = user.folders

    folders_schema = FolderSchema(many=True)
    return jsonify(folders_schema.dump(folders)), 200


# get folder
@bp.route('/folders/<int:folder_id>', methods=['GET'])
@jwt_required
@swag_from('apidocs/get_folder.yml')
def get_folder(folder_id):
    user = get_current_user()
    try:
        folder = user.folders.filter(Folder.id == folder_id).one()
    except NoResultFound:
        return jsonify(msg='Folder not found for this user'), 404
    folder_schema = FolderSchema()
    return jsonify(folder_schema.dump(folder)), 200


# create folder
@bp.route('/folders', methods=['POST'])
@jwt_required
@swag_from('apidocs/create_folder.yml', endpoint='api.create_folder')
@folder_validation
def create_folder(name, parent_id, user):
    new_folder = Folder(name=name, parent_id=parent_id, user_id=user.id)
    new_folder.save_to_db()

    folder_schema = FolderSchema()
    return jsonify(folder_schema.dump(new_folder)), 200


# update folder
@bp.route('/folders/<int:folder_id>', methods=['PUT'])
@jwt_required
@swag_from('apidocs/update_folder.yml')
@folder_validation
def update_folder(folder_id, name, parent_id, user):
    try:
        folder = user.folders.filter(Folder.id == folder_id).one()
    except NoResultFound:
        return jsonify(msg='Folder not found for this user'), 404
    folder.name = name
    folder.parent_id = parent_id
    folder.save_to_db()

    # return jsonify(msg='Success'), 200
    folder_schema = FolderSchema()
    return jsonify(folder_schema.dump(folder)), 200


# delete folder
@bp.route('/folders/<int:folder_id>', methods=['DELETE'])
@jwt_required
@swag_from('apidocs/delete_folder.yml')
def delete_folder(folder_id):
    user = get_current_user()
    try:
        folder = user.folders.filter(Folder.id == folder_id).one()
    except NoResultFound:
        return jsonify(msg='Folder not found for this user'), 404
    if not folder.parent_id:
        return jsonify(msg='Can not delete root folder'), 403
    folder_schema = FolderSchema()
    dump = folder_schema.dump(folder)
    folder.delete()

    return jsonify(dump), 200
