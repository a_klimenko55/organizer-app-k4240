import os
from flasgger import Swagger
from flask import Flask, redirect, url_for
from flask_migrate import Migrate
from flask_sqlalchemy import SQLAlchemy
from flask_marshmallow import Marshmallow
from flask_jwt_extended import JWTManager, jwt_required
from dotenv import load_dotenv
import json

swagger_config = {
        "headers": [
        ],
        "specs": [
            {
                "endpoint": 'apispec_1',
                "route": '/apispec_1.json',
                "rule_filter": lambda rule: True,  # all in
                "model_filter": lambda tag: True,  # all in
            }
        ],
        "static_url_path": "/flasgger_static",
        # "static_folder": "static",  # must be set by user
        "swagger_ui": True,
        "specs_route": "/apidocs/",
        "openapi": "3.0.2",
        'uiversion': 3
    }


app = Flask(__name__)
load_dotenv()
with open(app.root_path+'/static/flasgger_schemas.json', 'r') as fp:
    template = json.load(fp)
app.config['SQLALCHEMY_DATABASE_URI'] = os.environ.get('DATABASE_URL')
app.config['JWT_TOKEN_LOCATION'] = ['headers', 'cookies']
app.config['JWT_ACCESS_TOKEN_EXPIRES'] = 60*60*24
app.config['JWT_COOKIE_CSRF_PROTECT'] = False
app.config['JWT_SECRET_KEY'] = 'super-secret'
app.config['JWT_BLACKLIST_ENABLED'] = True
app.config['JWT_BLACKLIST_TOKEN_CHECKS'] = ['access', 'refresh']
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
db = SQLAlchemy(app)
migrate = Migrate(app, db)
ma = Marshmallow(app)
jwt = JWTManager(app)
swagger = Swagger(app, config=swagger_config, decorators=[jwt_required], template=template)


@app.route('/', methods=['GET', 'POST'])
def index():
    return redirect(url_for('auth.apidocs_login'))


from project.api import bp as api_bp
app.register_blueprint(api_bp, url_prefix='/api/1.0')

from project.auth import bp as auth_bp
app.register_blueprint(auth_bp, url_prefix='/auth/')

from project import models, schemas, blacklist_helpers
